﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class LVL1Controller : NetworkBehaviour {
    public CollisonDetect collisonDetect1;
    public CollisonDetect collisonDetect2;
    public CollisonDetect door;
    public Pickup pickup;
    public ParticleSystem particle;
    public float waterneeded = 10;
    public GameObject tree;
    public bool grown;
    public Material mat;
    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        if (collisonDetect1.touchingPlayer == true && collisonDetect2.touchingPlayer == true)
        {
            particle.Play();
            waterneeded = waterneeded - 1 * Time.deltaTime;
        }
        else
        {
            particle.Stop();
        }
        if (grown == false && waterneeded < 0) {
            tree.transform.localScale = new Vector3(1,1,1);
            tree.transform.Translate(0, 0, 1.376f);
            tree.GetComponent<MeshRenderer>().material = mat;
            grown = true;
        }
        if (pickup.PickedUp == true && door.touchingPlayer == true)
        {
            
            SceneManager.LoadScene("Menu");
        }
	}
}
