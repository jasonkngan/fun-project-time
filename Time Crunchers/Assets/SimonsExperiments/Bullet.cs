﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody>().AddForce(Vector3.right * 100, ForceMode.Impulse);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
