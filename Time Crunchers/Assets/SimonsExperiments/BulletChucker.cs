﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletChucker : MonoBehaviour {

    public GameObject Bullet;
	// Use this for initialization
	void Start () {
        InvokeRepeating("Spawn", 0.5f, 0.5f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void Spawn()
    {
        Instantiate(Bullet, transform.position, Quaternion.identity);
    }
}
