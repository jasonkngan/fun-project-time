﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glass : MonoBehaviour {
    public GameObject ShatteredGlass;
    public Vector3 posOffset;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.name == "Bullet(Clone)")
        {
            var glazz = Instantiate(ShatteredGlass, transform.position + posOffset, transform.rotation);
            Destroy(glazz, 10.0f);
            Destroy(gameObject);
        }
    }
}
