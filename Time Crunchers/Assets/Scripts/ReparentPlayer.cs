﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReparentPlayer : MonoBehaviour {
    public Transform Parent;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Player") {
            collision.transform.parent = Parent;
            //gameObject.SetActive(false);
        }
    }
}
