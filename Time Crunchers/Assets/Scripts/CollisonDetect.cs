﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisonDetect : MonoBehaviour {
    public bool touchingPlayer = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnCollisionStay(Collision collision)
    {
        if (collision.transform.tag == "Player")
        {
            touchingPlayer = true;
        }
    }

    public void OnCollisionExit(Collision collision)
    {
        if (collision.transform.tag == "Player")
        {
            touchingPlayer = false;
        }
    }
}
