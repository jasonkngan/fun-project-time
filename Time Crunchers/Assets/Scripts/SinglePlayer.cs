using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinglePlayer : MonoBehaviour {
	private Rigidbody rig;
	private Vector3 velocity;
	private GameObject cameraz;
	float x;
	float y;
	public float sensetivity = 2;
	public float speed = 1;
	public Transform bulletSpawn;
	public GameObject bulletPrefab;
	private CapsuleCollider Colid;
	public float tp;
	public int Maxdim;
	public int Mindim;
	public int Curdim;
	public bool jumper;
	public float jump = 6f;
	// Use this for initialization
	void Start () {
		cameraz = transform.Find ("Main Camera").gameObject;
		cameraz.GetComponent<AudioListener> ().enabled = true;
		rig = GetComponent<Rigidbody> ();
		Colid = GetComponent<CapsuleCollider> ();
		cameraz.GetComponent<Camera> ().depth = 5;
	}

	// Update is called once per frame
	void Update () {
		Screen.lockCursor = true;
		//Moving
		x = sensetivity * Input.GetAxis ("Mouse X");
		y = -Mathf.Clamp (sensetivity * Input.GetAxis ("Mouse Y"), -45, 45);

		velocity = new Vector3 (Input.GetAxisRaw ("Horizontal") * speed, 0, Input.GetAxisRaw ("Vertical")).normalized * speed;
		transform.Rotate (0, x, 0);
		cameraz.transform.Rotate (y,0,0);	
		if (Input.GetKeyDown(KeyCode.Space) && jumper)
		{
			rig.AddForce (Vector3.up * jump, ForceMode.Impulse);
			jumper = false;
		}

		if (Input.GetKeyDown(KeyCode.Q) && Curdim < Maxdim)
		{
			transform.position = new Vector3 (transform.position.x + tp ,transform.position.y,transform.position.z);
			Curdim = Curdim + 1;
		}
		if (Input.GetKeyDown(KeyCode.E) && Curdim > Mindim)
		{
			transform.position = new Vector3 (transform.position.x - tp ,transform.position.y,transform.position.z);
			Curdim = Curdim - 1;
		}
	}
	void FixedUpdate(){
		rig.MovePosition (rig.position + transform.forward * velocity.z * Time.fixedDeltaTime);
		rig.MovePosition (rig.position + transform.right * velocity.x * Time.fixedDeltaTime);
		if (Input.GetKey (KeyCode.LeftShift)) {
			Colid.height = 1;
		} else {
			Colid.height = 2;
		}
	}
	void OnCollisionEnter(Collision col){
		jumper = true;
		if (col.transform.tag == "Finish") {
			Destroy (gameObject);
		}
		if (col.transform.tag == "Bullet") {
			rig.constraints = RigidbodyConstraints.None;
		}
	}

	/*
	[Command]
	void CmdFire()
	{
		// Create the Bullet from the Bullet Prefab
		var bullet = (GameObject)Instantiate(
			bulletPrefab,
			bulletSpawn.position,
			bulletSpawn.rotation);

		// Add velocity to the bullet
		bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 20;

		// Spawn the bullet on the Clients
		NetworkServer.Spawn(bullet);

		// Destroy the bullet after 2 seconds
		Destroy(bullet, 10.0f);
	}
	*/
}
