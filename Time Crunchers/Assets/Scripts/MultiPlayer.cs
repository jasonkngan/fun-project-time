using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MultiPlayer : NetworkBehaviour {
	private Rigidbody rig;
	private Vector3 velocity;
	private GameObject cameraz;
	float x;
	float y;
	public float sensetivity = 2;
	public float speed = 1;
	public Transform bulletSpawn;
	public GameObject bulletPrefab;
	private CapsuleCollider Colid;
	public float tp;
	public bool jumper;
	public float jump = 6f;
    public Material[] mats;
    public Material mat;
    bool StartTimer = false;
    float Timer = 0;
    public GameObject shield;
    public AudioSource Sound;
    public AudioClip hit;
    public AudioClip Died;
    public float health = 100;
    // Use this for initialization
    void Start () {
        gameObject.GetComponent<MeshRenderer>().material = mats[Random.Range(0, mats.Length)];
        if (!isLocalPlayer)
        {
            return;
        }
		cameraz = transform.Find ("Main Camera").gameObject;
		cameraz.GetComponent<AudioListener> ().enabled = true;
		rig = GetComponent<Rigidbody> ();
		Colid = GetComponent<CapsuleCollider> ();
		cameraz.GetComponent<Camera> ().depth = 5;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        Screen.lockCursor = true;

        if (health < 0)
        {
            rig.constraints = RigidbodyConstraints.None;
            Sound.PlayOneShot(Died);
        }
        else
        {
            //Moving
            x = sensetivity * Input.GetAxis("Mouse X");
            y = -Mathf.Clamp(sensetivity * Input.GetAxis("Mouse Y"), -45, 45);

            velocity = new Vector3(Input.GetAxisRaw("Horizontal") * speed, 0, Input.GetAxisRaw("Vertical")).normalized * speed;
            transform.Rotate(0, x, 0);
            cameraz.transform.Rotate(y, 0, 0);
            if (Input.GetKeyDown(KeyCode.Space) && jumper)
            {
                rig.AddForce(transform.up * jump, ForceMode.Impulse);
                jumper = false;
            }

            if (Input.GetKeyDown(KeyCode.Q))
            {
                shield.SetActive(true);

                StartTimer = true;
            }

            if (StartTimer == true)
            {
                Timer = Timer + 1 * Time.deltaTime;
            }

            if (Timer >= 5)
            {
                shield.SetActive(false);
                Timer = 0;

            }
            if (Input.GetMouseButtonDown(0))
            {
                CmdFire();
            }
        }
        Sound.pitch = Random.Range(1f,1.5f);
    }
	void FixedUpdate(){
        if (!isLocalPlayer)
        {
            return;
        }
        if (health < 0)
        {
        }
        else {
            rig.MovePosition(rig.position + transform.forward * velocity.z * Time.fixedDeltaTime);
            rig.MovePosition(rig.position + transform.right * velocity.x * Time.fixedDeltaTime);
            if (Input.GetKey(KeyCode.LeftShift))
            {
                Colid.height = 1;
            }
            else
            {
                Colid.height = 2;
            }

            transform.localEulerAngles = (new Vector3(0, transform.localEulerAngles.y, 0));
        }
	}
	void OnCollisionEnter(Collision col){
        if (!isLocalPlayer)
        {
            return;
        }
        jumper = true;
		if (col.transform.tag == "Finish") {
			Destroy (gameObject);
		}
		if (col.transform.tag == "Bullet") {
            if (transform.GetComponent<Rigidbody>().velocity.magnitude > 1)
            {
                health = health - transform.GetComponent<Rigidbody>().velocity.magnitude;
                Sound.PlayOneShot(hit);
            }
        }
	}

	
	[Command]
	void CmdMatPeristance()
	{
        gameObject.GetComponent<MeshRenderer>().material = mat;
    }

    [Command]
    void CmdFire()
    {
        // Create the Bullet from the Bullet Prefab
        var bullet = (GameObject)Instantiate(
            bulletPrefab,
            bulletSpawn.position,
            bulletSpawn.rotation);

        // Add velocity to the bullet
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 40;

        // Spawn the bullet on the Clients
        NetworkServer.Spawn(bullet);

        // Destroy the bullet after 2 seconds
        Destroy(bullet, 10.0f);
    }
}
