﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipe : MonoBehaviour {
    public CollisonDetect collisonDetect1;
    public CollisonDetect collisonDetect2;
    public ParticleSystem particle;
    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        if (collisonDetect1.touchingPlayer == true && collisonDetect2.touchingPlayer == true)
        {
            particle.Play();
        }
        else
        {
            particle.Stop();
        }
	}
}
